import React, { useState, useEffect } from "react";
import { Item } from './item';
import { Data } from './data';
import './app.scss'
import jsonData from './data.json'

export default function App() {
    const data: Data[] = jsonData
    const [items, setItems] = useState<Item[]>([])

    useEffect(() => {
        const calculateAndSetItems = (data: Data[]) => {
            const maxAmount: number = data.reduce((max: Data, current: Data) => {
                return max.amount > current.amount ? max : current
            }, data[0]).amount
            const convertedItems: Item[] = data.map((element: Data) => ({
                heightPct: Math.floor(element.amount * 100.0 / maxAmount),
                name: element.day,
                tooltip: `$${element.amount}`
            }))
            setItems(convertedItems)
        }

        calculateAndSetItems(data);
    }, [data])

    return (<div className={'app'}>
        <div className={'container'}>
            <div className={'header'}>
                <div className={'my-balance-header'}>
                    <label className={'my-balance-label'}>My balance</label>
                    <label className={'balance'}>$921.48</label>
                </div>
                <img src={'logo.svg'} alt={''}/>
            </div>
            <div className={'main-container'}>
                <label className={'spending-label'}>Spending - Last 7 days</label>
                <div className={'data-container'}>
                    {items.map(item => (<div className={'data-item-container'}>
                        <div className={'data-bar'} style={{ height: `${item.heightPct}%` }}></div>
                        <div className={'data-item-name'}>{item.name}</div>
                    </div>))}
                </div>
                <hr/>
            </div>
        </div>
    </div>)
}
