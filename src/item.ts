export interface Item {
    heightPct: number;
    name: string;
    tooltip: string;
}
